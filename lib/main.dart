import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tic_tac_toe/HomeScreen.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (_) {
        return MaterialApp(
          // initialRoute: '/root',
          // routes: {
          //   '/root': (context) => const HomeScreen(),
          // },
          theme: ThemeData(
            textTheme: TextTheme(
              headline4: TextStyle(
                  color: Colors.white,
                  fontSize: 24.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          title: "Tic Tac Toe",
          home: HomeScreen(),
        );
      },
    );
  }
}
