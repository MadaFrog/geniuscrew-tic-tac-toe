import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  /// set the board size to be [_gridSize] X [_gridSize]
  final int _gridSize = 3;

  /// used to keep track of the values in the squares of the board
  late List<List<String>> board;

  /// used to keep track of the state of the game
  late GameState _gameState;

  @override
  void initState() {
    super.initState();
    _initGame();
  }

  void _initGame() {
    /// fill the board with empty values
    board =
        List.generate(_gridSize, (x) => List.generate(_gridSize, (y) => ""));

    /// initialise game state
    /// set y as the last player to move because, theoretically,
    /// X should start and the last move was made by Y
    _gameState = GameState("Y", Winner.none, false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: _gameState.lastTurn == "X"
            ? Colors.lightBlue.withOpacity(0.5)
            : Colors.amber[700]?.withOpacity(0.7),
        body: Column(
          children: [
            SizedBox(height: 40.h),

            /// SCREEN TITLE
            _gameState.winner == Winner.X
                ? Text(
                    "X won the game",
                    style: Theme.of(context).textTheme.headline4,
                  )
                : _gameState.winner == Winner.Y
                    ? Text(
                        "Y won the game",
                        style: Theme.of(context).textTheme.headline4,
                      )
                    : _gameState.ended
                        ? Text(
                            "It's a tie!".toUpperCase(),
                            style: Theme.of(context).textTheme.headline4,
                          )
                        : SizedBox(height: 30.sp),

            SizedBox(height: 80.h),

            const Spacer(),

            /// GAME BOARD
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                child: ScrollConfiguration(
                  behavior: NoMoreGlow(), // remove glow from scrolling
                  child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    // make sure can't scroll the board
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: _gridSize,
                    ),
                    itemBuilder: _showSquare,

                    /// set grid shape to square
                    itemCount: _gridSize * _gridSize,
                  ),
                ),
              ),
            ),
            const Spacer(),

            _gameState.ended
                ? ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _initGame();
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      primary: _gameState.lastTurn == "X"
                          ? Colors.lightBlue
                          : Colors.amber,
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                          horizontal: 50.r, vertical: 10.r),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                    ),
                    child: Text(
                      "Restart",
                      style: TextStyle(color: Colors.black, fontSize: 24.sp),
                    ),
                  )
                : SizedBox(height: 50.sp),

            const Spacer(),
          ],
        ),
      ),
    );
  }

  Widget _showSquare(context, index) {
    /// get the row and column index
    int x = (index / _gridSize).floor();
    int y = (index % _gridSize);

    return Padding(
      padding: EdgeInsets.all(5.r),
      child: ElevatedButton(
        onPressed: () => !_gameState.ended ? _squarePressed(x, y) : null,
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.transparent,
          onSurface: Colors.transparent,
          primary: board[x][y] == ""
              ? Colors.white
              : board[x][y] == "X"
                  ? Colors.amber
                  : Colors.lightBlue,
          onPrimary: Colors.white,
          alignment: Alignment.center,
          padding: EdgeInsets.all(10.r),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.r),
          ),
        ),
        child: Text(
          board[x][y],
          style: TextStyle(
            fontSize: 26.sp,
          ),
        ),
      ),
    );
  }

  _squarePressed(int x, int y) {
    /// check if the button can be pressed
    /// so if the value is ""
    if (board[x][y] == "") {
      board[x][y] = _gameState.lastTurn == "X" ? "Y" : "X";
      if (!_checkGameWon(x, y)) {
        setState(() {
          _gameState.lastTurn = _gameState.lastTurn == "X" ? "Y" : "X";
        });
      }
    }
  }

  /// check if someone won the game and who
  bool _checkGameWon(int x, int y) {
    int n = _gridSize;
    int row = 0;
    int col = 0;
    int mainDiagonal = 0;
    int secondDiagonal = 0;

    debugPrint("Checking for square $x, $y. Turn: ${_gameState.lastTurn}");

    String _lastTurn = _gameState.lastTurn == "X" ? "Y" : "X";
    // String _lastTurn = _gameState.lastTurn;

    /// only have to search the board from the current selected square
    for (int i = 0; i < n; i++) {
      /// check rows
      if (board[x][i] == _lastTurn) row++;

      /// check columns
      if (board[i][y] == _lastTurn) col++;

      // check main diagonal
      if (board[i][i] == _lastTurn) mainDiagonal++;

      // check second diagonal
      if (board[i][n - i - 1] == _lastTurn) secondDiagonal++;
    }

    // debugPrint("Col: $col Row: $row Diag: $mainDiagonal SecondDiag: $secondDiagonal");

    if (row == n || col == n || mainDiagonal == n || secondDiagonal == n) {
      setState(() {
        _gameState.winner = _lastTurn == "X" ? Winner.X : Winner.Y;
        _gameState.ended = true;

        // _showEnd();
      });
      return true;
    } else if (!board.any((x) => x.contains(""))) {
      /// check if the board has any empty squares left
      /// if not, then it's a tie
      setState(() {
        _gameState.winner = Winner.none;
        _gameState.ended = true;

        // _showEnd();
      });
      return true;
    }

    return false;
  }

  /// shows and alert dialog with the winner if [_gameState.ended] is true
  /// or the message that it's a tie if [_gameState.ended] is false
  _showEnd() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: _gameState.winner == Winner.none
            ? const Text("It's a tie.")
            : Text("${_gameState.winner == Winner.X ? "X" : "Y"} won!"),
        content: const Text('Fancy another round?'),
        actions: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.black,
              onPrimary: Colors.amber,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
            ),
            onPressed: () {
              setState(() {
                /// restart the game
                _initGame();
              });

              /// close the dialog
              Navigator.of(context).pop();
            },
            child: const Text('Restart'),
          )
        ],
      ),
    );
  }
}

enum Winner {
  X,
  Y,
  none,
}

class GameState {
  String lastTurn;
  Winner winner;
  bool ended;

  GameState(this.lastTurn, this.winner, this.ended);
}

/// CLASS USED TO REMOVE THE SCROLL GLOW FROM LISTS
class NoMoreGlow extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
